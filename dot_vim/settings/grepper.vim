" Open the Ag command and place the cursor into the quotes
nmap ,ag :GrepperRg ""<Left>
nmap ,af :'<,'>Ack ""<Left>

let g:grepper = {}
let g:grepper.rg = { 'grepprg': 'rg --no-ignore-vcs --vimgrep -s --' }
"if executable('rg')
"  let g:ackprg = 'rg --vimgrep'
"endif
