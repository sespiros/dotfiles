function! GetVisual()
  let reg_save = getreg('"')
  let regtype_save = getregtype('"')
  let cb_save = &clipboard
  set clipboard&
  normal! ""gvy
  let selection = getreg('"')
  call setreg('"', reg_save, regtype_save)
  let &clipboard = cb_save
  return selection
endfunction

"grep the current word using ,k (mnemonic Kurrent)
nnoremap <silent> ,k :GrepperRg <cword><CR>

"grep visual selection
vnoremap ,k :<C-U>execute "GrepperRg " . GetVisual()<CR>

"grep for 'def foo'
nnoremap <silent> ,gd :GrepperRg 'def <cword>'<CR>

",gg = Grep! - using Ag the silver searcher
" open up a grep line, with a quote started for the search
nnoremap ,gg :GrepperRg ""<left>

"Grep for usages of the current file
nnoremap ,gcf :exec "GrepperRg " . expand("%:t:r")<CR>

"https://github.com/mhinz/vim-grepper/issues/74
let g:qf_mapping_ack_style = 1
