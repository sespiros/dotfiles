require('litee.lib').setup({
    tree = {
        icon_set = "codicons"
    },
    panel = {
        orientation = "right",
        panel_size  = 60
    },
})

-- sometimes the default behaviour which uses the quickfix may be preferable
require('litee.calltree').setup({
    icon_set = "codicons",
    -- hide_cursor = false,
    on_open = "panel"
})

-- require('litee.symboltree').setup({
--     on_open = "panel"
-- })
