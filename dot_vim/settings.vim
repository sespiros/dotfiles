let vimsettings = '~/.vim/settings'
let uname = system("uname -s")

for fpath in split(globpath(vimsettings, '*.vim'), '\n')

  if (fpath == expand(vimsettings) . "/yadr-keymap-mac.vim") && uname[:4] ==? "linux"
    continue " skip mac mappings for linux
  endif

  if (fpath == expand(vimsettings) . "/yadr-keymap-linux.vim") && uname[:4] !=? "linux"
    continue " skip linux mappings for mac
  endif

  exe 'source' fpath
endfor

lua require('litee')
lua require('lsp')
lua require('autodarkmode')
lua require('comment')

lua require('indent')
let g:indent_blankline_enabled = v:false

lua require('null')

" https://github.com/vim/vim/issues/2049
set mmp=5000


