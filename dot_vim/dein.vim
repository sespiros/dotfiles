if &compatible
  set nocompatible
endif
" Add the dein installation directory into runtimepath
set runtimepath+=~/.vim/bundles/repos/github.com/Shougo/dein.vim

" Workaround for https://github.com/wsdjeg/dein-ui.vim/issues/17
" let spacevim_update_retry_cnt = 0

if dein#load_state('~/.vim/bundles')
  call dein#begin('~/.vim/bundles')

  call dein#add('~/.vim/bundles/repos/github.com/Shougo/dein.vim')
  call dein#add('wsdjeg/dein-ui.vim')

  "-------------- Appearance -------------------------------------------------------
  call dein#add('chrisbra/Colorizer')
  call dein#add('skwp/vim-colors-solarized')
  call dein#add('itchyny/lightline.vim')
  call dein#add('xsunsmile/showmarks.git')
  call dein#add('vim-scripts/ScrollColors')
  call dein#add('danielfalk/auto-dark-mode.nvim')
  call dein#add('jtratner/vim-flavored-markdown')
  call dein#add('lukas-reineke/indent-blankline.nvim')

  "-------------- Git ---------------------------------------------------------------
  call dein#add('tpope/vim-fugitive')
  call dein#add('tpope/vim-git')

  "-------------- Programming -------------------------------------------------------
  " Snippets, maybe remove
  " call dein#add('tomtom/tlib_vim.git')
  " call dein#add('MarcWeber/vim-addon-mw-utils.git')
  " call dein#add('garbas/vim-snipmate.git')
  call dein#add('honza/vim-snippets')

  " Syntax highlight/checking etc.
  "call dein#add('sheerun/vim-polyglot')
  "call dein#add('scrooloose/syntastic.git')
  call dein#add('skwp/vim-html-escape')

  " ctags related
  call dein#add('vim-scripts/AutoTag.git')

  " lsp
  call dein#add('nvim-treesitter/nvim-treesitter', {'hook_post_update': 'TSUpdate'})
  call dein#add('neovim/nvim-lspconfig')
  call dein#add('hrsh7th/nvim-cmp')
  call dein#add('hrsh7th/cmp-nvim-lsp')
  call dein#add('saadparwaiz1/cmp_luasnip')
  call dein#add('L3MON4D3/LuaSnip')

  call dein#add('ldelossa/litee.nvim')
  call dein#add('ldelossa/litee-calltree.nvim')
  call dein#add('ldelossa/litee-symboltree.nvim')

  call dein#add('nvim-lua/plenary.nvim')
  call dein#add('jose-elias-alvarez/null-ls.nvim')

  " Auto-completion
  call dein#add('tpope/vim-endwise.git')
  " call dein#add('Shougo/deoplete.nvim')
  " call dein#add('Shougo/deoplete-clangx')

  " replace with LSP
  call dein#add('Keithbsmiley/investigate.vim')

  " maybe I should disable gopls integration if it's covered by nvim-lspconfig
  call dein#add('fatih/vim-go')

  "-------------- Project ----------------------------------------------------------
  call dein#add('jistr/vim-nerdtree-tabs.git')
  call dein#add('scrooloose/nerdtree.git')
  call dein#add('ctrlpvim/ctrlp.vim')
  call dein#add('JazzCore/ctrlp-cmatcher')
  call dein#add('preservim/tagbar.git')
  call dein#add('xolox/vim-misc')
  call dein#add('xolox/vim-session')
  "call dein#add('junegunn/fzf')

  "-------------- Search ----------------------------------------------------------
  " call dein#add('justinmk/vim-sneak')
  call dein#add('Lokaltog/vim-easymotion')
  call dein#add('mhinz/vim-grepper')
  call dein#add('henrik/vim-indexed-search')
  call dein#add('nelstrom/vim-visual-star-search')
  call dein#add('vim-scripts/camelcasemotion.git')
  " maybe remove?
  call dein#add('skwp/greplace.vim')

  "-------------- Text objects ----------------------------------------------------
  " These bundles introduce new textobjects into vim,
  " For example the Ruby one introduces the 'r' text object
  " such that 'var' gives you Visual Around Ruby
  call dein#add('austintaylor/vim-indentobject')
  call dein#add('bootleq/vim-textobj-rubysymbol')
  call dein#add('coderifous/textobj-word-column.vim')
  call dein#add('kana/vim-textobj-datetime')
  call dein#add('kana/vim-textobj-entire')
  call dein#add('kana/vim-textobj-function')
  call dein#add('kana/vim-textobj-user')
  call dein#add('lucapette/vim-textobj-underscore')
  call dein#add('thinca/vim-textobj-function-javascript')
  call dein#add('wellle/targets.vim')

  "-------------- Vim improvements ------------------------------------------------
  call dein#add('mbbill/undotree')
  call dein#add('AndrewRadev/splitjoin.vim')
  call dein#add('Raimondi/delimitMate')
  call dein#add('briandoll/change-inside-surroundings.vim.git')
  call dein#add('numToStr/Comment.nvim')
  call dein#add('bogado/file-line.git')
  call dein#add('christoomey/vim-tmux-navigator')
  call dein#add('vim-scripts/lastpos.vim')
  call dein#add('goldfeld/ctrlr.vim')
  call dein#add('fidian/hexmode')
  call dein#add('wellle/context.vim')
  call dein#add('Yilin-Yang/vim-markbar')
  call dein#add('romainl/vim-qf')
  call dein#add('svermeulen/vim-yoink')
  "call dein#add('svermeulen/vim-cutlass')

  call dein#add('tpope/vim-surround.git')
  call dein#add('tpope/vim-ragtag')
  call dein#add('tpope/vim-repeat.git')
  call dein#add('tpope/vim-unimpaired')

  " remove second
  call dein#add('godlygeek/tabular')
  call dein#add('tpope/vim-abolish')
  call dein#add('wincent/terminus')

  " remove first
  call dein#add('editorconfig/editorconfig-vim')
  call dein#add('chrisbra/NrrwRgn')
  call dein#add('vim-scripts/sudo.vim')
  call dein#add('vim-scripts/vis')

  " remove candidates
  "call dein#add('vim-scripts/AnsiEsc.vim.git')

  call dein#end()
  call dein#save_state()
endif

if dein#check_install()
    call dein#install()
    let g:pluginsExists=1
endif

filetype plugin indent on
syntax enable
