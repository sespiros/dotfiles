export XDG_CURRENT_DESKTOP=river
export _JAVA_AWT_WM_NONREPARENTING=1
export QT_QPA_PLATFORM=wayland
export QT_WAYLAND_DISABLE_WINDOWDECORATION="1"

if [ -n "$DESKTOP_SESSION" ]; then
    export SSH_AUTH_SOCK=/run/user/1000/keyring/ssh
fi
