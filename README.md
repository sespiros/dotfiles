# My dotfiles

## Usage

1. Install curl and zsh

2. Install chezmoi and apply configuration
```
sh -c "$(curl -fsLS git.io/chezmoi)" -- init --apply https://gitlab.com/sespiros/dotfiles
```
