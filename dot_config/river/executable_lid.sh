#!/bin/sh

if [ $(cat /sys/class/power_supply/AC/online) -eq 1 ]; then
    if $(cat /proc/acpi/button/lid/LID/state | grep -q open); then
        wlr-randr --output eDP-1 --on;
    else
        wlr-randr --output eDP-1 --off;
    fi
else
    systemctl suspend;
fi
