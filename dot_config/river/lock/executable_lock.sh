#!/bin/bash
cd "${HOME}/.config/river/lock"

notify-send --urgency low "Locking..."

padpng=/home/sespiros/.config/river/lock/pad.png

lock1=/tmp/lock1.png
grim -o eDP-1 $lock1 > /dev/null
convert $lock1 -scale 25% -blur 0x20 -scale 400% $lock1;
composite -gravity center $padpng $lock1 $lock1;

lock2=/tmp/lock2.png
grim -o DP-1 $lock2 > /dev/null
if [ $? -eq 0 ]; then
    convert $lock2 -scale 25% -blur 0x20 -scale 400% $lock2;
    composite -gravity center $padpng $lock2 $lock2;
fi
