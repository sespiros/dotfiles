#!/bin/bash

dim() {
    if [ "$1" == "low" ]; then
        /usr/bin/brightnessctl set 0%
        ddcutil setvcp 10 0
    elif [ "$1" == "high" ]; then
        /usr/bin/brightnessctl set 100%
        ddcutil setvcp 10 80
    elif [ "$1" == "on" ]; then
        wlr-randr --output eDP-1 --on
        wlr-randr --output DP-1 --on
        /usr/bin/brightnessctl set 100%
        ddcutil setvcp 10 80
    elif [ "$1" == "off" ]; then
        wlr-randr --output eDP-1 --off
        wlr-randr --output DP-1 --off
    else
        echo "Usage: $0 <low|high|on|off>"
        exit 1
    fi
}

dim "$1"
